# 插件WebSocket说明

## WebSocket类定义
    WebSocket类只需要使用@ServerEndpoint注解即可，无需其他额外注解

## WebSocket访问路径
### 无参数路径
    ws://ip:port/basic-example-plugin3/test/no_path_param
### 有参数路径
    ws://ip:port/basic-example-plugin3/test/has_path_param/xxx