package com.basic.example.plugin3;

import com.gitee.starblues.realize.BasePlugin;
import org.pf4j.PluginWrapper;

/**
 * 插件定义类
 * websocket 测试网站：http://coolaf.com/tool/chattest
 * @author starBlues
 * @version 1.0
 */
public class DefinePlugin extends BasePlugin {
    public DefinePlugin(PluginWrapper wrapper) {
        super(wrapper);
    }

    @Override
    protected void startEvent() {

    }

    @Override
    protected void deleteEvent() {

    }

    @Override
    protected void stopEvent() {

    }
}
